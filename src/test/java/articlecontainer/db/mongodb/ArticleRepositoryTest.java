package articlecontainer.db.mongodb;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import articlecontainer.config.MongoConfig;
import articlecontainer.domain.Article;

@ContextConfiguration(classes = MongoConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ArticleRepositoryTest {

	private static final int PAGE_SIZE = 5;
	private static final int PAGE_WITH_NEWEST_ARTICLES = 0;

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	MongoOperations mongoOps;

	@Before
	public void cleanup() {
		// Deleting all articles (just in case something is left over from a
		// previous failed run)
		articleRepository.deleteAll();
	}

	@Test
	public void testMongoRepository() {
		assertEquals(0, articleRepository.count());
		Article article = createAnArticle();

		// Saving an article
		Article savedArticle = articleRepository.save(article);
		assertEquals(1, articleRepository.count());

		// Finding an article by ID
		Article foundArticle = articleRepository.findOne(savedArticle.getId());
		assertEquals("Witalis Maxwell", foundArticle.getAuthor());
		assertEquals(4, foundArticle.getTags().size());

		// Finding an article by a single field value
		List<Article> maxwellsArticles = articleRepository.getByAuthor("Witalis Maxwell");
		assertEquals(1, maxwellsArticles.size());
		assertEquals("Witalis Maxwell", maxwellsArticles.get(0).getAuthor());
		assertEquals(4, maxwellsArticles.get(0).getTags().size());

		// Finding an article by a single field value like
		List<Article> witalisLikeArticles = articleRepository.findByAuthorLike("Witalis");
		assertEquals(1, witalisLikeArticles.size());
		assertEquals("Witalis Maxwell", witalisLikeArticles.get(0).getAuthor());
		assertEquals(4, witalisLikeArticles.get(0).getTags().size());

		// Finding article list for home page
		Page<Article> articlePage = articleRepository
				.findByDateCreatedNotNullOrderByDateCreatedDesc(new PageRequest(PAGE_WITH_NEWEST_ARTICLES, PAGE_SIZE));
		List<Article> articleListForHomePage = articlePage.getContent();
		assertEquals(1, articleListForHomePage.size());
		assertEquals("Give people courage", articleListForHomePage.get(0).getTitle());

		// Deleting an article
		articleRepository.delete(savedArticle.getId());
		assertEquals(0, articleRepository.count());
	}

	private Article createAnArticle() {
		Article article = new Article();
		article.setAuthor("Witalis Maxwell");
		article.setDateCreated(LocalDateTime.now());
		article.setLead("The crowd seemed to grow");
		article.setMainText("The sunset faded to twilight before anything further happened. "
				+ "The crowd far away on the left, towards Woking, seemed to grow, and I heard "
				+ "now a faint murmur from it. The little knot of people towards Chobham dispersed. "
				+ "There was scarcely an intimation of movement from the pit.");
		Set<String> tags = new HashSet<>();
		Collections.addAll(tags, "sunset", "crowd", "Chobham", "novel");
		article.setTags(tags);
		article.setTitle("Give people courage");
		return article;
	}
}
