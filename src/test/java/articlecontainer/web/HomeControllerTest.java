package articlecontainer.web;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import articlecontainer.db.mongodb.ArticleRepository;
import articlecontainer.domain.Article;

public class HomeControllerTest {

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	ArticleRepository mockRepository;

	@InjectMocks
	HomeController controller;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	}

	@Test
	public void testHomePage() throws Exception {

		List<Article> expectedArticles = createArticleList(5);

		when(mockRepository.findByDateCreatedNotNullOrderByDateCreatedDesc(any(PageRequest.class)).getContent())
				.thenReturn(expectedArticles);

		mockMvc.perform(get("/")).andExpect(view().name("home"))
				.andExpect(model().attributeExists("homePageArticleViewList"));
	}

	@Test
	public void anyPageBackPageSessionAttributeShouldBeLastVisitedPageNumber() throws Exception {
		List<Article> expectedArticles = createArticleList(5);

		when(mockRepository.findByDateCreatedNotNullOrderByDateCreatedDesc(any(PageRequest.class)).getContent())
				.thenReturn(expectedArticles);

		mockMvc.perform(get("/page/3")).andExpect(view().name("home"))
				.andExpect(model().attributeExists("homePageArticleViewList"))
				.andExpect(request().sessionAttribute("backPage", "3"));
	}

	private List<Article> createArticleList(int count) {
		Stream<Article> articles = Stream.generate(Article::new).limit(count);
		return articles.map(a -> {
			a.setMainText(Long.toHexString(ThreadLocalRandom.current().nextLong()));
			a.setDateCreated(LocalDateTime.now());
			return a;
		}).collect(Collectors.toList());
	}
}
