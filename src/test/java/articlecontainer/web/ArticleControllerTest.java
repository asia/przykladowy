package articlecontainer.web;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import articlecontainer.db.mongodb.ArticleRepository;
import articlecontainer.domain.Article;

public class ArticleControllerTest {
	
    @Mock
    ArticleRepository mockRepository;
	
    @InjectMocks
    ArticleController controller;
    
    private MockMvc mockMvc;
    
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }
    
    @Test
    public void failUpdatingArticleByProvidingWrongParamsForValidation() throws Exception {
    	Article articleToUpdate = createArticle();
    	
    	when(mockRepository.save(any(Article.class)))
    	.thenReturn(articleToUpdate);

        mockMvc.perform(post("/article/edit/testID")
	        		.param("title", "NO")//validation error
	        		.param("lead","max250")
	        		.param("mainText", "!min10")//validation error
	        		.param("citation", "max100characters")
	        		.param("author", "max30characters"))
        		.andExpect(model().hasErrors())
        		.andExpect(model().attributeHasErrors("article"))
        		.andExpect(status().isOk())
        		.andExpect(view().name("editArticle"));
    }

    @Test
    public void updateExistingArticleSuccessfully() throws Exception {
    	Article articleToUpdate = createArticle();
    	
    	when(mockRepository.save(any(Article.class)))
    	.thenReturn(articleToUpdate);

        mockMvc.perform(post("/article/edit/testID")
	        		.param("title", "titleMin3Max50")
	        		.param("lead","max250")
	        		.param("mainText", "min10characters")
	        		.param("citation", "max100characters")
	        		.param("author", "max30characters"))
        		.andExpect(model().hasNoErrors())
        		.andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/article/"+articleToUpdate.getId()));
    }
    
    @Test
    public void getArticleToEditWhichIsPresentInRepository() throws Exception {
    	Article articleToEdit = createArticle();
    	when(mockRepository.findOne("testID"))
    		.thenReturn(articleToEdit);
    	
    	mockMvc.perform(get("/article/edit/testID"))
    		.andExpect(status().isOk())
    		.andExpect(view().name("editArticle"))
            .andExpect(model().attributeExists("article"))
            .andExpect(model().attribute("article", articleToEdit));
    }
    
    @Test
    public void getArticleToEditWhichNotExistsOrRemoved() throws Exception {
    	mockMvc.perform(get("/article/edit/0000"))
    		.andExpect(view().name("error/articleNotFound"));
    }
    
    @Test
    public void deleteExistingArticleEndsWithRedirection() throws Exception {
    	mockMvc.perform(get("/article/delete/testID"))
    		.andExpect(status().is3xxRedirection())
    		.andExpect(redirectedUrl("/"));
    }
    
    @Test
    public void toCreateArticleFormMustNotBeEmpty() throws Exception {
        mockMvc.perform(post("/article/add"))
        	.andExpect(status().isOk())
        	.andExpect(model().attributeHasErrors("article"))
    		.andExpect(model().hasErrors())
            .andExpect(view().name("editArticle"));
    }
    
    @Test
    public void shouldNotPassValidationWhenPostingInvalidDataForArticle() throws Exception {
        mockMvc.perform(post("/article/add")
        		.param("title", "NO")
        		.param("lead","OKmax250")
        		.param("mainText", "FAILmin10")
        		.param("citation", "OKmax100characters")
        		.param("author", "FAILmax30charactersButHasMore!max30charactersButHasMore"))
        	.andExpect(status().isOk())
        	.andExpect(model().attributeHasErrors("article"))
    		.andExpect(model().hasErrors())
            .andExpect(view().name("editArticle"));
    }
    
    @Test
    public void createArticleSuccessfully() throws Exception {
    	Article savedArticle = createArticle();
    	
    	when(mockRepository.save(any(Article.class)))
    	.thenReturn(savedArticle);
    	
        mockMvc.perform(post("/article/add")
	        		.param("title", "titleMin3Max50")
	        		.param("lead","max250")
	        		.param("mainText", "min10characters")
	        		.param("citation", "max100characters")
	        		.param("author", "max30characters"))
        		.andExpect(model().hasNoErrors())
        		.andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/article/"+savedArticle.getId()));
    }
    
    @Test
	public void viewExistingArticleTest() throws Exception {
    	Article expectedArticle = createArticle();
    	when(mockRepository.findOne("testID"))
    	.thenReturn(expectedArticle);
    	
    	mockMvc.perform(get("/article/testID"))
    	.andExpect(status().isOk())
        .andExpect(view().name("article"))
        .andExpect(model().attributeExists("article"))
        .andExpect(model().attribute("article", expectedArticle));
    }
    
    @Test
	public void tryViewArticleWhichNotExistsShouldResultInErrorPage() throws Exception {
    	mockMvc.perform(get("/article/0000"))
    	.andExpect(view().name("error/articleNotFound"))
    	.andExpect(model().attributeDoesNotExist("article"))
    	.andExpect(model().attributeDoesNotExist("dateCreatedFormatedStr"));
    }
    
	public Article createArticle() {
		Article article = new Article();
		article.setId("testID");
		article.setDateCreated(LocalDateTime.now());
		return article;
	}
}
