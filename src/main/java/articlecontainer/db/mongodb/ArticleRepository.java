package articlecontainer.db.mongodb;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import articlecontainer.domain.Article;
import articlecontainer.domain.Category;

public interface ArticleRepository extends MongoRepository<Article, String> {

	List<Article> getByAuthor(String authorEqual);

	List<Article> findByAuthorLike(String authorLike);

	List<Article> getByCategory(Category category);

	List<Article> getByTags(List<String> tag);

	List<Article> getByDateCreated(LocalDateTime dateCreated);

	List<Article> getByDateModified(LocalDateTime dateModified);

	List<Article> findByDateCreatedBetween(LocalDateTime dateStart, LocalDateTime dateEnd);

	List<Article> findByDateModifiedBetween(LocalDateTime dateStart, LocalDateTime dateEnd);

	List<Article> getByCategoryOrderByDateCreated(Category category);

	List<Article> findByDateCreatedBetweenOrderByDateCreatedDesc(LocalDateTime dateStart, LocalDateTime dateEnd);

	List<Article> findByDateCreatedNotNullOrderByDateCreatedDesc();

	Page<Article> findByDateCreatedNotNullOrderByDateCreatedDesc(Pageable pageable);

	// for ehcache & method level security:

	@Override
	@Cacheable("articlecontainerCache")
	Article findOne(String id);

	@Override
	@CachePut(value = "articlecontainerCache", key = "#result.id")
	@RolesAllowed("CONTENT")
	<S extends Article> S save(S entity);

	@Override
	@CacheEvict(value = "articlecontainerCache")
	@RolesAllowed("CONTENT")
	void delete(String id);

}
