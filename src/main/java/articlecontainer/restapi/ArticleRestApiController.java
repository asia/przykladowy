package articlecontainer.restapi;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import articlecontainer.db.mongodb.ArticleRepository;
import articlecontainer.domain.Article;
import articlecontainer.web.exception.ArticleNotFoundException;

@RestController
@RequestMapping("/rest")
public class ArticleRestApiController {

	private ArticleRepository articleRepository;

	@Autowired
	public ArticleRestApiController(ArticleRepository articleRepository) {
		this.articleRepository = articleRepository;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public Article articleById(@PathVariable String id) {
		Article article = articleRepository.findOne(id);
		if (article == null) {
			throw new ArticleNotFoundException(id);
		}
		return article;
	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public List<Article> articles() {
		return articleRepository.findByDateCreatedNotNullOrderByDateCreatedDesc();
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Article> saveArticle(@RequestBody Article article, UriComponentsBuilder ucb) {
		Article savedArticle = articleRepository.save(article);

		HttpHeaders headers = new HttpHeaders();
		URI locationUri = ucb.path("/rest/").path(String.valueOf(savedArticle.getId())).build().toUri();
		headers.setLocation(locationUri);

		ResponseEntity<Article> responseEntity = new ResponseEntity<Article>(savedArticle, headers, HttpStatus.CREATED);
		return responseEntity;
	}

	@ExceptionHandler(ArticleNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public RestError articleNotFound(ArticleNotFoundException exception) {
		String articleId = exception.getArticleId();
		return new RestError(404, "Article [" + articleId + "] not found");
	}

}
