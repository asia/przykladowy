package articlecontainer.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Article Not Found")
public class ArticleNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 2985561940756507312L;
	
	private String articleId;
	
	public ArticleNotFoundException(String articleId){
		this.articleId = articleId;
	}
	
	public ArticleNotFoundException() {}

	public String getArticleId(){
		return articleId;
	}

}
