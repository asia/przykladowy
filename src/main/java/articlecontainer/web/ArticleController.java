package articlecontainer.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import articlecontainer.db.mongodb.ArticleRepository;
import articlecontainer.domain.Article;
import articlecontainer.web.exception.ArticleNotFoundException;

@Controller
@RequestMapping("/article")
public class ArticleController {

	private ArticleRepository articleRepository;

	@Autowired
	private ArticleController(ArticleRepository articleRepository) {
		this.articleRepository = articleRepository;
	}

	@RequestMapping(value = "/edit/{articleId}", method = GET)
	public String showAndPopulateEditArticleForm(@PathVariable String articleId, Model model) {

		Article article = getArticle(articleId);

		model.addAttribute(article);
		return "editArticle";
	}

	@RequestMapping(value = "/edit/{articleId}", method = POST)
	public String processArticleEdition(@Valid Article article, Errors errors) {

		if (errors.hasErrors()) {
			return "editArticle";
		}

		article.setDateModified(LocalDateTime.now());
		Article saved = articleRepository.save(article);

		return "redirect:/article/" + saved.getId();
	}

	@RequestMapping(value = "/add", method = GET)
	public String showAddNewArticleForm(Model model) {

		model.addAttribute(new Article());
		return "editArticle";
	}

	@RequestMapping(value = "/add", method = POST)
	public String processAddingNewArticle(@Valid Article article, Errors errors) {

		if (errors.hasErrors()) {
			return "editArticle";
		}
		article.setDateCreated(LocalDateTime.now());

		/*
		 * artilce id must be set to null, because we reuse jsp form for
		 * create/edit article and for editing article, we have id in '<input
		 * type=hidden' and this element gives empty but not null id for adding
		 * new article in this method; we could avoide this by making redundant
		 * jsp or by additional hit to database in processArticleEdition method,
		 * so this is best solution
		 */
		article.setId(null);

		Article savedArticle = articleRepository.save(article);
		return "redirect:/article/" + savedArticle.getId();
	}

	@RequestMapping(value = "/{articleId}", method = GET)
	public String showArticle(@PathVariable String articleId, Model model, HttpServletRequest request) {

		Article article = getArticle(articleId);

		model.addAttribute(article);
		LocalDateTime dateCreated = article.getDateCreated();
		String dateCreatedFormatedStr = dateCreated.format(DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy"));
		model.addAttribute("dateCreatedFormatedStr", dateCreatedFormatedStr);
		return "article";
	}

	@RequestMapping(value = "/delete/{articleId}", method = GET)
	public String deleteArticle(@PathVariable String articleId) {
		articleRepository.delete(articleId);
		return "redirect:/";
	}

	@ExceptionHandler(ArticleNotFoundException.class)
	public String handleNotFoundArticle() {
		return "error/articleNotFound";
	}

	Article getArticle(String articleId) {

		Article article = articleRepository.findOne(articleId);
		if (article == null) {
			throw new ArticleNotFoundException();
		}
		return article;
	}

}
