package articlecontainer.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import articlecontainer.db.mongodb.ArticleRepository;
import articlecontainer.domain.Article;
import articlecontainer.domain.HomePageArticleView;

@Controller
@RequestMapping("/")
public class HomeController {

	private static final int PAGE_SIZE = 5;
	private static final int PAGE_WITH_NEWEST_ARTICLES = 0;

	private ArticleRepository articleRepository;

	@Autowired
	private HomeController(ArticleRepository articleRepository) {
		this.articleRepository = articleRepository;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String home(Model model, HttpServletRequest request) {
		request.getSession().removeAttribute("backPage");
		generateArticlesForView(PAGE_WITH_NEWEST_ARTICLES, model);
		return "home";
	}

	/**
	 * the pageNumber which is presented for application user, for internal
	 * paging must be subtracted by 1
	 */
	@RequestMapping(value = "page/{pageNumber}", method = RequestMethod.GET)
	public String articles(@PathVariable String pageNumber, Model model, HttpServletRequest request) {

		request.getSession().setAttribute("backPage", pageNumber);

		int pageNumberInt = PAGE_WITH_NEWEST_ARTICLES;

		try {
			pageNumberInt = Integer.valueOf(pageNumber) - 1;
		} catch (NumberFormatException ex) {
			pageNumberInt = PAGE_WITH_NEWEST_ARTICLES;
		}

		generateArticlesForView(pageNumberInt, model);

		return "home";
	}

	private void generateArticlesForView(Integer pageNumber, Model model) {

		if (pageNumber == null || pageNumber < 0) {
			pageNumber = PAGE_WITH_NEWEST_ARTICLES;
		}

		List<Article> articleList;

		Page<Article> articlePage = articleRepository
				.findByDateCreatedNotNullOrderByDateCreatedDesc(new PageRequest(pageNumber, PAGE_SIZE));
		articleList = articlePage.getContent();

		int numberOfPages = articlePage.getTotalPages();
		int numberOfArticles = (int) articlePage.getTotalElements();

		model.addAttribute("numberOfPages", numberOfPages);
		model.addAttribute("numberOfArticles", numberOfArticles);

		List<HomePageArticleView> homeArticleList = 
				articleList.stream()
							.map(HomePageArticleView::new)
							.collect(Collectors.toList());

		model.addAttribute(homeArticleList);
	}
}
