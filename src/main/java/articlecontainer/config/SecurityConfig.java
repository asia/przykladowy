package articlecontainer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;

@Configuration
@EnableWebSecurity
public class SecurityConfig  extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	
	  http
	    .formLogin()
	    .and()
	      .logout()
	        .logoutSuccessUrl("/")
	    .and()
	    .rememberMe()
	      .tokenRepository(new InMemoryTokenRepositoryImpl())
	      .tokenValiditySeconds(2419200)
	      .key("adminKey")
	    .and()
	     .httpBasic()
	       .realmName("articlecontainer")
	    .and()
	    .authorizeRequests()
	      .antMatchers("/article/edit/*").authenticated()
	      .antMatchers("/article/add").authenticated()
	      .antMatchers("/article/delete/*").authenticated()
	      .anyRequest().permitAll();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
				.withUser("admin").password("admin").roles("CONTENT");
	}

	@Bean(name = "myAuthenticationManager")
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

}
