package articlecontainer.domain;

import java.time.LocalDateTime;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Article {

	@Id
	private String id;

	@NotNull(message = "{title.validation.notNull}")
	@Size(min = 3, max = 50, message = "{title.validation.size}")
	private String title;

	@Size(max = 250, message = "{lead.validation.size}")
	private String lead;

	@NotNull(message = "{mainText.validation.notNull}")
	@Size(min = 10, message = "{mainText.validation.size}")
	private String mainText;

	@Size(max = 100, message = "{citation.validation.size}")
	private String citation;

	@NotNull(message = "{author.validation.notNull}")
	@Size(max = 30, message = "{author.validation.size}")
	private String author;

	private Category category;

	private Set<String> tags;

	private LocalDateTime dateCreated;

	private LocalDateTime dateModified;

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLead() {
		return lead;
	}

	public void setLead(String lead) {
		this.lead = lead;
	}

	public String getMainText() {
		return mainText;
	}

	public void setMainText(String mainText) {
		this.mainText = mainText;
	}

	public String getCitation() {
		return citation;
	}

	public void setCitation(String citation) {
		this.citation = citation;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	public LocalDateTime getDateModified() {
		return dateModified;
	}

	public void setDateModified(LocalDateTime dateModified) {
		this.dateModified = dateModified;
	}
}
