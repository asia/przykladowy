package articlecontainer.domain;

import java.time.format.DateTimeFormatter;

public final class HomePageArticleView {

	private static final int MAX_ARTICLE_LENGTH_FOR_HOMEPAGE = 1220;
	private String id;
	private String title;
	private String lead;
	private String mainText;
	private String citation;
	private String dateCreated;
	private String author;
	
	public HomePageArticleView(Article article){
		this.id = article.getId();
		this.title = article.getTitle(); 
		this.lead = article.getLead();
		this.mainText = article.getMainText().length() > MAX_ARTICLE_LENGTH_FOR_HOMEPAGE
						? article.getMainText().substring(0, MAX_ARTICLE_LENGTH_FOR_HOMEPAGE) + "..."
						: article.getMainText();
		this.citation = article.getCitation(); 
		this.dateCreated = article.getDateCreated().format(DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy"));
		this.author = article.getAuthor();
	}

	public HomePageArticleView(String id, String title, String lead, String mainText, String citation,
			String dateCreated, String author) {
		super();
		this.id = id;
		this.title = title;
		this.lead = lead;
		this.mainText = mainText;
		this.citation = citation;
		this.dateCreated = dateCreated;
		this.author = author;
	}

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getLead() {
		return lead;
	}

	public String getMainText() {
		return mainText;
	}

	public String getCitation() {
		return citation;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public String getAuthor() {
		return author;
	}
}
