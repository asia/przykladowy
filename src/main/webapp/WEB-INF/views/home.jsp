<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="content">

    <div class="collumns">
    	<c:forEach items="${homePageArticleViewList}" var="article" varStatus="styleIndex">
    	<s:url value="/article/${article.id}" var="see_article_url" />
    	
        <div class="collumn">
        
        
        	<div class="head">
        	<c:choose>
        		<c:when test="${styleIndex.index == 0 }">
        			<span class="headline hl3">
        		</c:when>
        		<c:when test="${styleIndex.index == 1 }">
        			<span class="headline hl5">
        		</c:when>
        		<c:when test="${styleIndex.index == 2 }">
        			<span class="headline hl1">
        		</c:when>
        		<c:when test="${styleIndex.index == 3 }">
        			<span class="headline hl3">
        		</c:when>
        		<c:when test="${styleIndex.index == 4 }">
        			<span class="headline hl1">
        		</c:when>
        		<c:otherwise>
        			<span class="headline hl3">
        		</c:otherwise>
        	</c:choose>
        	<c:out value="${article.title}" />
        	</span>
        	
        	<p>
        	<c:choose>
        		<c:when test="${styleIndex.index == 0 }">
        			<span class="headline hl4">
        		</c:when>
        		<c:when test="${styleIndex.index == 1 }">
        			<span class="headline hl6">
        		</c:when>
        		<c:when test="${styleIndex.index == 2 }">
        			<span class="headline hl2">
        		</c:when>
        		<c:when test="${styleIndex.index == 3 }">
        			<span class="headline hl4">
        		</c:when>
        		<c:when test="${styleIndex.index == 4 }">
        			<span class="headline hl4">
        		</c:when>
        		<c:otherwise>
        			<span class="headline hl4">
        		</c:otherwise>
        	</c:choose>
        	
        		<c:choose>
				    <c:when test="${not empty article.lead}">
				        <c:out value="${article.lead}" />
				    </c:when>
	        		<c:otherwise>
	        			<s:message code="jsp.views.home.by" /> <c:out value="${article.author}" />
	        		</c:otherwise>
				</c:choose>
				
        	</span></p></div>

<c:choose>
<c:when test='${fn:contains(article.mainText, "<c>") and not empty article.citation}'>
<span  style="white-space: pre-wrap;"><c:out value='${fn:substringBefore(article.mainText, "<c>")}' /></span>
<span class="citation"><c:out value="${article.citation}" /></span>
<span  style="white-space: pre-wrap;"><c:out value='${fn:substringAfter(article.mainText, "<c>")}' /></span>
</c:when>
<c:otherwise>
<span  style="white-space: pre-line;"><c:out value='${article.mainText}' /></span>
</c:otherwise>
</c:choose>
			<a href="${see_article_url}" style="color:Grey">
        		<s:message code="jsp.views.home.readMore" />
        	</a>   	
        </div>

        
        </c:forEach>
    </div>

</div>


<div style="margin:auto; display:table;">
<s:message code="jsp.views.home.goTo" />
<c:forEach var="page" begin="1" end="${numberOfPages}">
<a href="/page/${page}" style="color:Grey"><s:message code="jsp.views.home.goToPage" /> ${page}</a>| 
</c:forEach>
</div>
