<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<h1><s:message code="jsp.views.editArticle.pageHeader"/></h1>

<sf:form method="POST" commandName="article">
	<sf:hidden path="id"/>
	<sf:hidden path="dateCreated"/>
	<sf:errors path="*" element="div" cssClass="errors" />
	<sf:label path="title" cssErrorClass="error"><s:message code="jsp.views.editArticle.title"/></sf:label>: <br/>
    <sf:input path="title" cssStyle="width: 50%;" cssErrorClass="error" />
	<br />
	<sf:label path="lead" cssErrorClass="error"><s:message code="jsp.views.editArticle.lead"/></sf:label>: <br/>
    <sf:textarea cssStyle="white-space: pre-wrap; width: 50%;" path="lead" cssErrorClass="error" />
	<br />
	<sf:label path="mainText" cssErrorClass="error"><s:message code="jsp.views.editArticle.mainText"/></sf:label>: <br/>
    <sf:textarea cssStyle="white-space: pre-wrap; width: 50%; height:200px;" path="mainText" cssErrorClass="error" />
	<br />
	<sf:label path="citation" cssErrorClass="error"><s:message code="jsp.views.editArticle.citation"/></sf:label>: <br/>
    <sf:textarea cssStyle="white-space: pre-wrap; width: 50%; height:50px;" path="citation" cssErrorClass="error" />
	<br />
	<sf:label path="author" cssErrorClass="error"><s:message code="jsp.views.editArticle.author"/></sf:label>: 
    <sf:input path="author" cssStyle="width: 20%" cssErrorClass="error" />
	<br /><br/>
	<a href="<s:url value="/" />"><s:message code="jsp.views.editArticle.back"/></a>
	<input type="submit" value="Save" />
</sf:form>
