<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<div class="notfounderror">
	<div class="message"><s:message code="jsp.views.articleNotFound.errorInfo" /></div>
	<a href="<s:url value="/" />" class="backurl"> 
		<s:message code="jsp.views.articleNotFound.return" />
	</a>
</div>
