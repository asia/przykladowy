<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<center>

 <div>
<span class="articleviewtitle">
<c:out value="${article.title}"/>
</span>
<br/><span><c:out value="${dateCreatedFormatedStr}"/></span><br/><br/><br/>
</div>

<c:choose>
<c:when test='${fn:contains(article.mainText, "<c>") and not empty article.citation}'>
<div  class="multicolumn"><c:if test="${not empty article.lead}"><span class="lead"><c:out value="${article.lead}" /></span></c:if><c:out value='${fn:substringBefore(article.mainText, "<c>")}' /><span class="citation" style="float: right; margin:auto, 10px; width:40%; display:table;"><c:out value="${article.citation}" /></span><c:out value='${fn:substringAfter(article.mainText, "<c>")}' />
<c:if test="${not empty article.author}"><br/><div style="text-align: right;"><b><c:out value="${article.author}" /></b></div></c:if></div>
</c:when>
<c:otherwise>
<div  class="multicolumn"><c:if test="${not empty article.lead}"><span class="lead"><c:out value="${article.lead}" /></span></c:if><c:out value='${article.mainText}' />
<c:if test="${not empty article.author}"><br/><div style="text-align: right;"><b><c:out value="${article.author}" /></b></div></c:if></div>

</c:otherwise>
</c:choose>

</center>

<br/><br/>
<sec:authorize access="hasRole('CONTENT')">
<s:url value="/article/edit/${article.id}" var="edit_article_url" />
<s:url value="/article/delete/${article.id}" var="delete_article_url" />
<a href="${edit_article_url}"><s:message code="jsp.views.article.editLink"/></a>
<a href="${delete_article_url}" onclick="return confirm('Delete permanently?');"><s:message code="jsp.views.article.deleteLink"/></a>
</sec:authorize>

<c:if test="${empty backPage}">
<a href="<s:url value="/" />"  style="color:Grey;"><s:message code="jsp.views.article.backLink"/></a>
</c:if>
<c:if test="${not empty backPage}">
<a href="<s:url value="/page/${backPage}" />"  style="color:Grey;"><s:message code="jsp.views.article.backLink"/></a>
</c:if>


