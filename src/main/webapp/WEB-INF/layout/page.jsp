<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="robots" content="noindex">
<link
	href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic|Droid+Serif:400,700,400italic,700italic'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="/resources/newspaper.css">
<link rel="icon" type="image/png" href="/resources/images/favico/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="/resources/images/favico/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="/resources/images/favico/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="/resources/images/favico/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="/resources/images/favico/favicon-128.png" sizes="128x128" />
<title><s:message code="jsp.layout.page.head.title"/></title>
<meta name="viewport" content="width=device-width">
</head>
<body>
	<div id="header">
		<t:insertAttribute name="header" />
	</div>
	<div id="content">
		<t:insertAttribute name="body" />
	</div>
	<div id="footer">
		<t:insertAttribute name="footer" />
	</div>
</body>
</html>