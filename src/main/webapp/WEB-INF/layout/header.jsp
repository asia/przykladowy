<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<a href="<s:url value="/" />" style="color:Grey;"><s:message code="jsp.layout.header.homeLink"/></a>
<sec:authorize access="hasRole('CONTENT')">
<a href="<c:url value="/article/add" />" style="color:Grey"><s:message code="jsp.layout.header.addArticleLink"/></a>
</sec:authorize>
 
<div class="head">
	<div class="headerobjectswrapper">
		<div class="weatherforcastbox" style="display: none;">
			<span style="font-style: italic;">Weatherforcast for the next
				24 hours: Plenty of Sunshine</span><br> <span>Wind: 7km/h SSE;
				Ther: 21°C; Hum: 82%</span>
		</div>
		<header><s:message code="jsp.layout.header.head.header.webPageTitle"/></header>
	</div>

<% 
java.time.LocalDate today = java.time.LocalDate.now(); 
String todayFormatedStr = 
today.format(java.time.format.DateTimeFormatter.ofPattern("EEEE, dd MMMM yyyy", new java.util.Locale("pl")));
%>

	<div class="subhead"><s:message code="jsp.layout.header.head.subhead.webPageSubtitle"/> <%=todayFormatedStr%></div>
</div>
