<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<br/>
<sec:authorize access="isAuthenticated()">
<sf:form action="${pageContext.request.contextPath}/logout" method="POST">
      <input name="logout" type="submit" value="Logout" />
</sf:form>
</sec:authorize>
